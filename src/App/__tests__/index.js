import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import toJson from 'enzyme-to-json';

import App from '../';

configure({ adapter: new Adapter() });

describe(App.name, () => {
  test(`it renders correctly`, () => {
    const component = shallow(<App />);

    const tree = toJson(component);

    expect(tree).toMatchSnapshot();
  });
});

/*

describe('addProduct', function scope() {
  beforeEach(() => {
    this.App = new App();
  });

  test('Should add a quantity of the product', () => {
    this.App.state = {
      productList: [
        {
          reference: 'Reference 1',
          quantity: 42,
        },
      ],
      basket: [],
      addedProductsNumber: 0,
    };

    this.App.setState = jest.fn(fn => {
      expect(fn(this.App.state)).toMatchSnapshot();
    });

    this.App.addProduct('Reference 1', 1);
  });

  test('Should add the quantity to the product quantity if the product is already added', () => {
    this.App.state = {
      basket: [
        {
          reference: 'Reference 1',
          quantity: 42,
        },
        {
          reference: 'Reference 2',
          quantity: 8,
        },
      ],
      addedProductsNumber: 50,
    };

    this.App.setState = jest.fn(fn => {
      expect(fn(this.App.state)).toMatchSnapshot();
    });

    this.App.addProduct('Reference 1', 1);
  });
});

describe('moreQuantity', function scope() {
  beforeEach(() => {
    this.App = new App();
  });

  test('Should add 1 to the product quantity', () => {
    this.App.state = {
      basket: [
        {
          reference: 'Reference 1',
          quantity: 42,
        },
        {
          reference: 'Reference 2',
          quantity: 8,
        },
      ],
      addedProductsNumber: 50,
    };

    this.App.setState = jest.fn(fn => {
      expect(fn(this.App.state)).toMatchSnapshot();
    });

    this.App.moreQuantity('Reference 1');
  });
});

describe('deleteProduct', function scope() {
  beforeEach(() => {
    this.App = new App();
  });

  test('Should delete the product', () => {
    this.App.state = {
      basket: [
        {
          reference: 'Reference 1',
          quantity: 42,
        },
        {
          reference: 'Reference 2',
          quantity: 8,
        },
      ],
      addedProductsNumber: 50,
    };

    this.App.setState = jest.fn(fn => {
      expect(fn(this.App.state)).toMatchSnapshot();
    });

    this.App.deleteProduct('Reference 1');
  });
});

describe('lessQuantity', function scope() {
  beforeEach(() => {
    this.App = new App();
  });

  test('Should remove 1 to the product quantity', () => {
    this.App.state = {
      basket: [
        {
          reference: 'Reference 1',
          quantity: 42,
        },
        {
          reference: 'Reference 2',
          quantity: 8,
        },
      ],
      addedProductsNumber: 50,
    };

    this.App.setState = jest.fn(fn => {
      expect(fn(this.App.state)).toMatchSnapshot();
    });

    this.App.lessQuantity('Reference 1');
  });

  test('Should delete the product if the quantity is equal to 1', () => {
    this.App.state = {
      basket: [
        {
          reference: 'Reference 1',
          quantity: 1,
        },
        {
          reference: 'Reference 2',
          quantity: 8,
        },
      ],
      addedProductsNumber: 9,
    };

    this.App.setState = jest.fn(fn => {
      expect(fn(this.App.state)).toMatchSnapshot();
    });

    this.App.lessQuantity('Reference 1');
  });
});

describe('validBasket', function scope() {
  beforeEach(() => {
    this.App = new App();
  });

  test('Should clear the basket', () => {
    this.App.state = {
      basket: [
        {
          reference: 'Reference 1',
          quantity: 42,
        },
        {
          reference: 'Reference 2',
          quantity: 8,
        },
      ],
      addedProductsNumber: 50,
    };

    this.App.setState = jest.fn(fn => {
      expect(fn(this.App.state)).toMatchSnapshot();
    });

    this.App.validBasket();
  });
});

*/
