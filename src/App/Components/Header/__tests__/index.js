import itRendersAllMutations from '../../../../../lib/it-renders-all-mutations';

import Header from '../header';

const mutations = [
  {
    name: 'with all props',
    props: {
      title: 'Title',
      quantity: 0,
    },
  },
];

describe(Header.name, () => {
  itRendersAllMutations(Header, mutations);
});
