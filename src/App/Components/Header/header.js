import React, { Component } from 'react';
import { number } from 'prop-types';
import { Link } from 'react-router-dom';

class Header extends Component {
  static propTypes = {
    quantity: number,
  };

  static defaultProps = {
    quantity: 0,
  };

  render = () => {
    const { quantity } = this.props;

    return (
      <div id="Header">
        <h1 id="Title">
          <Link to="/">Sushi discount</Link>
        </h1>
        <span id="Quantity">
          <Link to="/Basket">{quantity} product(s)</Link>
        </span>
      </div>
    );
  };
}

export default Header;
