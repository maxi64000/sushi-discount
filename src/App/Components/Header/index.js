import { connect } from 'react-redux';

import Header from './header';

const mapStateToProps = state => ({
  quantity: state.basket.reduce((sum, { quantity }) => sum + quantity, 0),
});

export default connect(
  mapStateToProps,
  null,
)(Header);
