import itRendersAllMutations from '../../../../../lib/it-renders-all-mutations';

import Button from '../';

const mutations = [
  {
    name: 'with all props',
    props: {
      title: 'Title',
      event: jest.fn(),
    },
  },
];

describe(Button.name, () => {
  itRendersAllMutations(Button, mutations);
});
