import React, { Component } from 'react';
import { string, func } from 'prop-types';

class Button extends Component {
  static propTypes = {
    title: string,
    event: func,
  };

  static defaultProps = {
    title: '',
    event: Function.prototype,
  };

  render = () => <button onClick={this.props.event}>{this.props.title}</button>;
}

export default Button;
