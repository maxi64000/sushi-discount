import React, { Component } from 'react';
import { array, func } from 'prop-types';

import Product from './Components/Product';

class ProductList extends Component {
  static propTypes = {
    list: array,
    setProductList: func,
  };

  static defaultProps = {
    list: [],
    setProductList: Function.prototype,
  };

  componentDidMount() {
    const { setProductList } = this.props;

    fetch('https://sdiscount-api.herokuapp.com/products')
      .then(response => response.json())
      .then(response => {
        setProductList(response);
      });
  }

  renderList = () => {
    const { list } = this.props;

    return list.map(({ title, reference, price }) => (
      <Product
        key={reference}
        reference={reference}
        title={title}
        price={price}
      />
    ));
  };

  render = () => (
    <div id="ProductList">
      <ul>{this.renderList()}</ul>
    </div>
  );
}

export default ProductList;
