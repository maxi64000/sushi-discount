import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import ProductList from './productList';
import { setProductList } from '../../../Redux/actions';

const mapStateToProps = state => ({
  list: state.productList,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setProductList,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProductList);
