import itRendersAllMutations from '../../../../../lib/it-renders-all-mutations';

import ProductList from '../productList';

beforeEach(function() {
  window.fetch = jest.fn().mockImplementation(() => new Promise(() => {}));
});

const mutations = [
  {
    name: 'with all props',
    props: {},
  },
];

describe(ProductList.name, () => {
  itRendersAllMutations(ProductList, mutations);
});
