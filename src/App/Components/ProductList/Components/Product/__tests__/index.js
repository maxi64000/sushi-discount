import itRendersAllMutations from '../../../../../../../lib/it-renders-all-mutations';

import Product from '../product';

const mutations = [
  {
    name: 'with all props',
    props: {
      title: 'Title',
      reference: 'Reference',
      price: 99,
    },
  },
];

describe(Product.name, () => {
  itRendersAllMutations(Product, mutations);
});

/*

describe('handlers', () => {
  describe('handleChange', function scope() {
    beforeEach(() => {
      this.Product = new Product();
    });

    test('it calls setState', () => {
      this.Product.setState = jest.fn(fn => {
        expect(fn(this.Product.state)).toMatchSnapshot();
      });
      this.Product.handleChange({
        target: {
          value: 1,
        },
      });

      expect(this.Product.setState).toHaveBeenCalled();
    });
  });

  describe('handleAddEvent', function scope() {
    beforeEach(() => {
      this.Product = new Product();
    });

    test('it calls addEvent', () => {
      this.Product.props = {
        reference: 'Reference',
        addEvent: jest.fn(),
      };

      this.Product.handleAddEvent();

      expect(this.Product.props.addEvent).toHaveBeenCalledWith('Reference', 1);
    });
  });
});

*/
