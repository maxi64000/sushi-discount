import React, { Component } from 'react';
import { func, string, number } from 'prop-types';

import Button from '../../../Button';

class Product extends Component {
  constructor() {
    super();

    this.state = {
      quantity: 1,
    };
  }

  static propTypes = {
    reference: string,
    title: string,
    price: number,
    addProduct: func,
  };

  static defaultProps = {
    reference: '',
    title: '',
    price: 0,
    addProduct: Function.prototype,
  };

  handleButtonClick = () => {
    const { reference, addProduct } = this.props;
    const { quantity } = this.state;

    addProduct(reference, quantity);
  };

  handleSelectChange = event => {
    const quantity = parseInt(event.target.value, 10);

    this.setState(() => ({
      quantity,
    }));
  };

  renderPrice = () => {
    const { price } = this.props;
    const { quantity } = this.state;

    const totalPrice = (quantity * price).toFixed(2);

    return <span>{totalPrice}€</span>;
  };

  renderQuantityList = () => {
    const quantityList = [1, 2, 3, 4, 5, 10, 20];

    return quantityList.map(quantity => (
      <option key={quantity} value={quantity}>
        {quantity}
      </option>
    ));
  };

  render = () => {
    const { reference, title } = this.props;

    return (
      <li key={reference}>
        {this.renderPrice()}
        <select onChange={this.handleSelectChange}>
          {this.renderQuantityList()}
        </select>
        <Button title="Add" event={this.handleButtonClick} />
        <span>{title}</span>
      </li>
    );
  };
}

export default Product;
