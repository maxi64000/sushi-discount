import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Product from './product';
import { addProduct } from '../../../../../Redux/actions';

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      addProduct,
    },
    dispatch,
  );

export default connect(
  null,
  mapDispatchToProps,
)(Product);
