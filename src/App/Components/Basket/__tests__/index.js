import itRendersAllMutations from '../../../../../lib/it-renders-all-mutations';

import Basket from '../basket';

const mutations = [
  {
    name: 'with all props',
    props: {
      title: 'Title',
      list: [
        {
          title: 'Product title',
          reference: 'Reference',
          quantity: 1,
          price: 99,
        },
      ],
      lessEvent: jest.fn(),
      moreEvent: jest.fn(),
      deleteEvent: jest.fn(),
    },
  },
];

describe(Basket.name, () => {
  itRendersAllMutations(Basket, mutations);
});
