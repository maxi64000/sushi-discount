import React, { Component } from 'react';
import { func, array } from 'prop-types';

import Product from './Components/Product';
import Button from '../Button';

class Basket extends Component {
  static propTypes = {
    list: array,
    clearBasket: func,
  };

  static defaultProps = {
    list: [],
    clearBasket: Function.prototype,
  };

  renderList = () => {
    const { list } = this.props;

    return list.map(({ title, reference, quantity, price }) => (
      <Product
        key={reference}
        title={title}
        reference={reference}
        quantity={quantity}
        price={price}
      />
    ));
  };

  renderTotalPrice = () => {
    const { list } = this.props;

    const totalPrice = list
      .reduce((sum, { quantity, price }) => sum + quantity * price, 0)
      .toFixed(2);

    return <span>Total de votre panier: {totalPrice}€</span>;
  };

  render = () => {
    const { title, clearBasket } = this.props;

    return (
      <div>
        <h2>{title}</h2>
        <ul>{this.renderList()}</ul>
        <div>
          {this.renderTotalPrice()}
          <Button title="Valider ma commande" event={clearBasket} />
        </div>
      </div>
    );
  };
}

export default Basket;
