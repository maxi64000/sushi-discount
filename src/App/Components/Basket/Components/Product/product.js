import React, { Component } from 'react';
import { func, string, number } from 'prop-types';

import Button from '../../../Button';

class Product extends Component {
  static propTypes = {
    title: string,
    reference: string,
    quantity: number,
    price: number,
    updateQuantity: func,
    deleteProduct: func,
  };

  static defaultProps = {
    title: '',
    reference: '',
    quantity: 0,
    price: 0,
    updateQuantity: Function.prototype,
    deleteProduct: Function.prototype,
  };

  handleRemoveQuantity = () => {
    const { reference, updateQuantity } = this.props;

    updateQuantity(reference, -1);
  };

  handleAddQuantity = () => {
    const { reference, updateQuantity } = this.props;

    updateQuantity(reference, 1);
  };

  handleDeleteProduct = () => {
    const { reference, deleteProduct } = this.props;

    deleteProduct(reference);
  };

  renderTotalPrice = () => {
    const totalPrice = (this.props.quantity * this.props.price).toFixed(2);

    return <span>{totalPrice}€</span>;
  };

  render = () => {
    const { reference, title, price, quantity } = this.props;

    return (
      <li key={reference}>
        <span>{title}</span>
        <span>{price.toFixed(2)}€</span>
        <div>
          <Button title="-" event={this.handleRemoveQuantity} />
          <input value={quantity} readOnly />
          <Button title="+" event={this.handleAddQuantity} />
        </div>
        {this.renderTotalPrice()}
        <Button title="Delete" event={this.handleDeleteProduct} />
      </li>
    );
  };
}

export default Product;
