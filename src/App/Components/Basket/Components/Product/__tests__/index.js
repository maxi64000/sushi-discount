import itRendersAllMutations from '../../../../../../../lib/it-renders-all-mutations';

import Product from '../product';

const mutations = [
  {
    name: 'with all props',
    props: {
      title: 'Title',
      reference: 'Reference',
      quantity: 1,
      price: 99,
    },
  },
];

describe(Product.name, () => {
  itRendersAllMutations(Product, mutations);
});

/*

describe('handlers', () => {
  describe('handleLessEvent', function scope() {
    beforeEach(() => {
      this.Product = new Product({});
    });

    test('it calls lessEvent', () => {
      this.Product.props = {
        reference: 'Reference',
        lessEvent: jest.fn(),
      };

      this.Product.handleLessEvent();

      expect(this.Product.props.lessEvent).toHaveBeenCalledWith('Reference');
    });
  });

  describe('handleMoreEvent', function scope() {
    beforeEach(() => {
      this.Product = new Product({});
    });

    test('it calls moreEvent', () => {
      this.Product.props = {
        reference: 'Reference',
        moreEvent: jest.fn(),
      };

      this.Product.handleMoreEvent();

      expect(this.Product.props.moreEvent).toHaveBeenCalledWith('Reference');
    });
  });

  describe('handleDeleteEvent', function scope() {
    beforeEach(() => {
      this.Product = new Product({});
    });

    test('it calls deleteEvent', () => {
      this.Product.props = {
        reference: 'Reference',
        deleteEvent: jest.fn(),
      };

      this.Product.handleDeleteEvent();

      expect(this.Product.props.deleteEvent).toHaveBeenCalledWith('Reference');
    });
  });
});

*/
