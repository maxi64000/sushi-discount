import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Product from './product';
import { updateQuantity, deleteProduct } from '../../../../../Redux/actions';

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateQuantity,
      deleteProduct,
    },
    dispatch,
  );

export default connect(
  null,
  mapDispatchToProps,
)(Product);
