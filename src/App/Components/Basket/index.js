import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Basket from './basket';
import { clearBasket } from '../../../Redux/actions';

const mapStateToProps = state => ({
  list: state.basket,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      clearBasket,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Basket);
