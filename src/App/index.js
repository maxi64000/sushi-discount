import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import Header from './Components/Header';
import ProductList from './Components/ProductList';
import Basket from './Components/Basket';

import './styles.css';

class App extends Component {
  render = () => (
    <div className="App">
      <Header />
      <Route exact path="/" component={ProductList} />
      <Route exact path="/Basket" component={Basket} />
    </div>
  );
}

export default App;
