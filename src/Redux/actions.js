import {
  SET_PRODUCTLIST,
  ADD_PRODUCT,
  UPDATE_QUANTITY,
  DELETE_PRODUCT,
  CLEAR_BASKET,
} from './types';

export const setProductList = productList => ({
  type: SET_PRODUCTLIST,
  payload: {
    productList,
  },
});

export const addProduct = (reference, quantity) => ({
  type: ADD_PRODUCT,
  payload: {
    reference,
    quantity,
  },
});

export const updateQuantity = (reference, quantity) => ({
  type: UPDATE_QUANTITY,
  payload: {
    reference,
    quantity,
  },
});

export const deleteProduct = reference => ({
  type: DELETE_PRODUCT,
  payload: {
    reference,
  },
});

export const clearBasket = () => ({
  type: CLEAR_BASKET,
});
