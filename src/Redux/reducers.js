import {
  SET_PRODUCTLIST,
  ADD_PRODUCT,
  UPDATE_QUANTITY,
  CLEAR_BASKET,
  DELETE_PRODUCT,
} from './types';

const initialState = {
  productList: [],
  basket: [],
};

const setProductList = (prevState, productList) => ({
  ...prevState,
  productList,
});

const getProduct = (reference, list) => {
  return list.find(product => product.reference === reference);
};

const addProduct = (prevState, reference, quantity = 1) => {
  const isAlreadyInBasket = prevState.basket.some(
    product => product.reference === reference,
  );

  if (!isAlreadyInBasket) {
    const product = getProduct(reference, prevState.productList);

    return {
      ...prevState,
      basket: [...prevState.basket, { ...product, quantity }],
    };
  } else {
    return updateQuantity(prevState, reference, quantity);
  }
};

const updateQuantity = (prevState, reference, quantity = 1) => {
  const basket = prevState.basket.map(product => {
    return product.reference === reference
      ? { ...product, quantity: product.quantity + quantity }
      : product;
  });

  const product = getProduct(reference, basket);

  if (product.quantity > 0) {
    return {
      ...prevState,
      basket,
    };
  } else {
    return deleteProduct(prevState, reference);
  }
};

const deleteProduct = (prevState, reference) => ({
  ...prevState,
  basket: prevState.basket.filter(product => product.reference !== reference),
});

const clearBasket = prevState => ({
  ...prevState,
  basket: initialState.basket,
});

export default (prevState = initialState, action) => {
  switch (action.type) {
    case SET_PRODUCTLIST:
      return setProductList(prevState, action.payload.productList);
    case ADD_PRODUCT:
      return addProduct(
        prevState,
        action.payload.reference,
        action.payload.quantity,
      );
    case DELETE_PRODUCT:
      return deleteProduct(prevState, action.payload.reference);
    case UPDATE_QUANTITY:
      return updateQuantity(
        prevState,
        action.payload.reference,
        action.payload.quantity,
      );
    case CLEAR_BASKET:
      return clearBasket(prevState);
    default:
      return prevState;
  }
};
