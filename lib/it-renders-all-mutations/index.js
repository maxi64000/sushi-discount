import itRendersCorrectly from '../it-renders-correctly';

export default (Component, mutations) => {
  describe('it renders all mutations', () => {
    mutations.forEach(({ name, props }) => {
      itRendersCorrectly(name, Component, props);
    });
  });
};
